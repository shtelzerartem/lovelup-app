import axios from 'axios';
import { host } from './host';
import { AsyncStorage } from 'react-native';

const errorCatcher = error => {
    console.log(error.response)
    if (error.response.status === 403 || error.response.data.error === "not found")
        window.location = '';
        AsyncStorage.multiRemove(["Token", "name", "gender", "age", 
                                  "preferred_gender", "min_age", "max_age"], (err) => {});
        return undefined;
    return error.response
}

const MyToken = async () => {
    try {
        const retrievedItem = await AsyncStorage.getItem("Token");
        return retrievedItem;
    } catch (error) {
        return error.message;
    }
};

const buildString = (params) => (
    params ? Object.keys(params).map((item, index) => ((index === 0 ? `?` : `&`) + `${item}=${params[item]}`)).join('') : ''
)

//                                                          //
//                                                          //
//                      GET REQUESTS                        //
//                                                          //
//                                                          //

// Register
function register(data, hostaddress = host){
    const request = `${hostaddress}register`;
    return axios.post(request, data).then(res => res).catch(error => errorCatcher(error))
}

// Update clients` data
const updateUser = async(data, hostaddress = host) => {
    const request = `${hostaddress}update_user`;
    return await MyToken().then(token => axios.post(request, data, {headers: {'Authorization': `Bearer ${token}`}}).then(res => res).catch(error => errorCatcher(error)))
}

// GetMe
const getMe = async(hostaddress = host) => {
    const request = `${hostaddress}me`;
    return await MyToken().then(token => axios.post(request, {}, {headers: {'Authorization': `Bearer ${token}`}}).then(res => res).catch(error => errorCatcher(error)))
}

const sendPicture = async(id, base64, hostaddress = host) => {
    const request = `${hostaddress}submit_task`;
    console.log(request); 
    
    return await MyToken().then(token => axios.post(request, {id: id, content: base64}, {headers: {'Authorization': `Bearer ${token}`, 'Content-Type': 'application/json'}}).then(res => res).catch(error => errorCatcher(error)))
}

export {
    register, updateUser, getMe, MyToken, sendPicture
}