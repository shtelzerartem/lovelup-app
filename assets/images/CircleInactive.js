import React from 'react';
import 
    Svg,
    {
        Ellipse,
        Defs,
        Stop,
        LinearGradient
    }
 from 'react-native-svg';

 export default function Circle(props){
    return (
        <Svg width="10" height="10" style={{marginRight: 5}}>
            <Defs>
                <LinearGradient id="circle_inactive_grad" x1="5" y1="0" x2="5" y2="10" gradientUnits="userSpaceOnUse">
                    <Stop offset="0" stopColor={props.active ? "#f4401d" : "#c6b7b7"} stopOpacity="1"/>
                    <Stop offset="1" stopColor={props.active ? "#c02642" : "#f5d7d7"} stopOpacity="1"/>
                </LinearGradient>
            </Defs>
            <Ellipse cx="5" cy="5" rx="5" ry="5" fill="url(#circle_inactive_grad)"/>
        </Svg>
    )
}
