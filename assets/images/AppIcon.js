import React, { PureComponent, Fragment } from 'react';
import 
    Svg,
    {
        Path,
        Circle,
        Stop, Defs, G,
        LinearGradient,
    }
 from 'react-native-svg';
 import { View, Animated, Easing } from 'react-native';

 export default function AppIcon1(){
    return (
        <Svg width="191" height="193" viewBox="0 0 191 193" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Path fillRule="evenodd" clipRule="evenodd" d="M115.158 78.5392C117.53 69.8789 117.704 60.4934 115.212 51.1942C107.725 23.2532 79.0054 6.67176 51.0645 14.1585C23.1235 21.6453 6.5421 50.3651 14.0289 78.3061C16.5206 87.6053 21.3641 95.6462 27.7481 101.96C27.9375 102.462 28.214 102.946 28.584 103.392L90.2053 177.795C92.9336 181.089 98.2699 179.659 98.9856 175.442L115.15 80.197C115.247 79.6253 115.245 79.0681 115.158 78.5392Z" fill="url(#paint0_linear)"/>
            <Path fillRule="evenodd" clipRule="evenodd" d="M163.725 101.783C170.109 95.4691 174.952 87.4282 177.444 78.1291C184.931 50.1881 168.349 21.4683 140.408 13.9815C112.467 6.49475 83.7476 23.0762 76.2608 51.0171C73.7691 60.3164 73.9434 69.7019 76.3151 78.3622C76.2283 78.8911 76.226 79.4483 76.323 80.02L92.4873 175.265C93.203 179.482 98.5393 180.912 101.268 177.618L162.889 103.215C163.259 102.769 163.535 102.285 163.725 101.783Z" fill="url(#paint1_linear)"/>
            <Circle cx="127.5" cy="60.5" r="16.5" fill="#4F1E31"/>
            <Defs>
                <LinearGradient id="paint0_linear" x1="51.0645" y1="14.1585" x2="95.3502" y2="179.435" gradientUnits="userSpaceOnUse">
                <Stop stopColor="#C6B7B7"/>
                <Stop offset="1" stopColor="#F5D7D7"/>
                </LinearGradient>
                <LinearGradient id="paint1_linear" x1="140.408" y1="13.9815" x2="96.1227" y2="179.258" gradientUnits="userSpaceOnUse">
                <Stop stopColor="#F4401D"/>
                <Stop offset="1" stopColor="#C02642"/>
                </LinearGradient>
            </Defs>
        </Svg>
    )

} 



// export default class AppIcon extends PureComponent{
//     constructor(props){
//         super(props);
//         this.path1 = new Animated.Value(0);
//         this.path2 = new Animated.Value(0);
//     }

//     componentDidMount(){
//         this.fadeIn();
//     }

//     fadeIn() {
//         this.path1.setValue(0);
//         Animated.timing(
//             this.path1,
//             {
//               toValue: 1,
//               duration: 500,
//               delay: 0,
//               easing: Easing.ease
//             }
//         ).start();
//         this.path2.setValue(0);
//         Animated.timing(
//             this.path2,
//             {
//               toValue: 1,
//               duration: 500,
//               delay: 0,
//               easing: Easing.ease
//             }
//         ).start();
//     }

//     render() {
//         const rotation1 = this.path1.interpolate({
//             inputRange: [0, 1],
//             outputRange: ['0deg', '30deg']
//         });
//         const translate1 = this.path1.interpolate({
//             inputRange: [0, 1],
//             outputRange: [0, 30]
//         });

//         const rotation2 = this.path2.interpolate({
//             inputRange: [0, 1],
//             outputRange: ['0deg', '-30deg']
//         });
//         const translate2 = this.path2.interpolate({
//             inputRange: [0, 1],
//             outputRange: [0, -30]
//         });
      
      
//         return (
//             <Fragment>
//                 <Animated.View style={{transform: [{rotate: rotation2, translateX: translate2}]}}>
//                     <Svg width="191" height="193" viewBox="0 0 191 193" fill="none" xmlns="http://www.w3.org/2000/svg">
//                         <G transform="rotate(15, 80, 200)">
//                             <Path fillRule="evenodd" clipRule="evenodd" d="M115.158 78.5392C117.53 69.8789 117.704 60.4934 115.212 51.1942C107.725 23.2532 79.0054 6.67176 51.0645 14.1585C23.1235 21.6453 6.5421 50.3651 14.0289 78.3061C16.5206 87.6053 21.3641 95.6462 27.7481 101.96C27.9375 102.462 28.214 102.946 28.584 103.392L90.2053 177.795C92.9336 181.089 98.2699 179.659 98.9856 175.442L115.15 80.197C115.247 79.6253 115.245 79.0681 115.158 78.5392Z" fill="url(#paint0_linear)"/>
//                         </G>
//                         <Defs>
//                         <LinearGradient id="paint0_linear" x1="51.0645" y1="14.1585" x2="95.3502" y2="179.435" gradientUnits="userSpaceOnUse">
//                         <Stop stopColor="#C6B7B7"/>
//                         <Stop offset="1" stopColor="#F5D7D7"/>
//                         </LinearGradient>
//                         <LinearGradient id="paint1_linear" x1="140.408" y1="13.9815" x2="96.1227" y2="179.258" gradientUnits="userSpaceOnUse">
//                         <Stop stopColor="#F4401D"/>
//                         <Stop offset="1" stopColor="#C02642"/>
//                         </LinearGradient>
//                     </Defs>
//                     </Svg>
//                 </Animated.View>
//                 <Animated.View style={{transform: [{rotate: rotation1}, {translateX: translate1}], position: 'absolute' }}>
//                 <Svg width="191" height="193" viewBox="0 0 191 193" fill="none" xmlns="http://www.w3.org/2000/svg">
//                     <G transform="rotate(-15, 120, 165)">
//                         <Path fillRule="evenodd" clipRule="evenodd" d="M163.725 101.783C170.109 95.4691 174.952 87.4282 177.444 78.1291C184.931 50.1881 168.349 21.4683 140.408 13.9815C112.467 6.49475 83.7476 23.0762 76.2608 51.0171C73.7691 60.3164 73.9434 69.7019 76.3151 78.3622C76.2283 78.8911 76.226 79.4483 76.323 80.02L92.4873 175.265C93.203 179.482 98.5393 180.912 101.268 177.618L162.889 103.215C163.259 102.769 163.535 102.285 163.725 101.783Z" fill="url(#paint1_linear)"/>
//                         <Circle cx="127.5" cy="60.5" r="16.5" fill="#4F1E31"/>
//                     </G>
//                     <Defs>
//                         <LinearGradient id="paint0_linear" x1="51.0645" y1="14.1585" x2="95.3502" y2="179.435" gradientUnits="userSpaceOnUse">
//                         <Stop stopColor="#C6B7B7"/>
//                         <Stop offset="1" stopColor="#F5D7D7"/>
//                         </LinearGradient>
//                         <LinearGradient id="paint1_linear" x1="140.408" y1="13.9815" x2="96.1227" y2="179.258" gradientUnits="userSpaceOnUse">
//                         <Stop stopColor="#F4401D"/>
//                         <Stop offset="1" stopColor="#C02642"/>
//                         </LinearGradient>
//                     </Defs>
//                 </Svg>
//                 </Animated.View>
//             </Fragment>
            
//         )
//     }
// } 

{/* <Svg width="191" height="193" viewBox="0 0 191 193" fill="none" xmlns="http://www.w3.org/2000/svg">
<Path fillRule="evenodd" clipRule="evenodd" d="M115.158 78.5392C117.53 69.8789 117.704 60.4934 115.212 51.1942C107.725 23.2532 79.0054 6.67176 51.0645 14.1585C23.1235 21.6453 6.5421 50.3651 14.0289 78.3061C16.5206 87.6053 21.3641 95.6462 27.7481 101.96C27.9375 102.462 28.214 102.946 28.584 103.392L90.2053 177.795C92.9336 181.089 98.2699 179.659 98.9856 175.442L115.15 80.197C115.247 79.6253 115.245 79.0681 115.158 78.5392Z" fill="url(#paint0_linear)"/>
<Animated.View style={{transform: [{rotate: rotation}] }}>
    <Path fillRule="evenodd" clipRule="evenodd" d="M163.725 101.783C170.109 95.4691 174.952 87.4282 177.444 78.1291C184.931 50.1881 168.349 21.4683 140.408 13.9815C112.467 6.49475 83.7476 23.0762 76.2608 51.0171C73.7691 60.3164 73.9434 69.7019 76.3151 78.3622C76.2283 78.8911 76.226 79.4483 76.323 80.02L92.4873 175.265C93.203 179.482 98.5393 180.912 101.268 177.618L162.889 103.215C163.259 102.769 163.535 102.285 163.725 101.783Z" fill="url(#paint1_linear)"/>
</Animated.View>
<Circle transform="rotate(-15, 120, 165)" cx="127.5" cy="60.5" r="16.5" fill="#4F1E31"/>
<Defs>
    <LinearGradient id="paint0_linear" x1="51.0645" y1="14.1585" x2="95.3502" y2="179.435" gradientUnits="userSpaceOnUse">
    <Stop stopColor="#C6B7B7"/>
    <Stop offset="1" stopColor="#F5D7D7"/>
    </LinearGradient>
    <LinearGradient id="paint1_linear" x1="140.408" y1="13.9815" x2="96.1227" y2="179.258" gradientUnits="userSpaceOnUse">
    <Stop stopColor="#F4401D"/>
    <Stop offset="1" stopColor="#C02642"/>
    </LinearGradient>
</Defs>
</Svg> */}