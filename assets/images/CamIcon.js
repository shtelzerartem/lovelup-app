import React from 'react';
import 
    Svg,
    {
        Path,
        Circle,
    }
 from 'react-native-svg';
 import { View, TouchableOpacity } from 'react-native';

const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    backgroundColor: "rgba(244, 64, 29, 0.1)",
    borderRadius: 50,
}

 export default function CamIcon(props){
    return (
        <TouchableOpacity style={shadowStyle} onPress={props.onPress}>
            <Svg width="93" height="93" viewBox="0 0 93 93" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Circle cx="46.5" cy="46.5" r="46.5" fill="#F4401D"/>
            <Path fill-rule="evenodd" clip-rule="evenodd" d="M57.0625 32.4167H63.6667C65.9583 32.4167 67.8333 34.2917 67.8333 36.5833V61.5833C67.8333 63.875 65.9583 65.75 63.6667 65.75H30.3333C28.0417 65.75 26.1667 63.875 26.1667 61.5833V36.5833C26.1667 34.2917 28.0417 32.4167 30.3333 32.4167H36.9375L40.75 28.25H53.25L57.0625 32.4167ZM30.3333 61.5833H63.6667V36.5833H55.2292L51.4167 32.4167H42.5833L38.7708 36.5833H30.3333V61.5833ZM47 38.6667C41.25 38.6667 36.5833 43.3333 36.5833 49.0833C36.5833 54.8333 41.25 59.5 47 59.5C52.75 59.5 57.4167 54.8333 57.4167 49.0833C57.4167 43.3333 52.75 38.6667 47 38.6667ZM40.75 49.0833C40.75 52.5208 43.5625 55.3333 47 55.3333C50.4375 55.3333 53.25 52.5208 53.25 49.0833C53.25 45.6458 50.4375 42.8333 47 42.8333C43.5625 42.8333 40.75 45.6458 40.75 49.0833Z" fill="white"/>
            </Svg>
        </TouchableOpacity>
    )
 }
