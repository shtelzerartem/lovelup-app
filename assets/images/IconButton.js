import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

function getSize(size) {
    switch(size){
        case 'big':
            return 56;
        case 'medim':
            return 48;
        case 'small':
            return 40
        default:
            return 48;
    }
}

const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    backgroundColor: "rgba(244, 64, 29, 0.1)",
    borderRadius: 50,
}

export default function IconButton(props){
    const size = getSize(props.size)

    return (
        <TouchableOpacity style={Object.assign({}, {
            display: 'flex',
            margin: 8,
        }, props.style ? props.style : {}, shadowStyle)} onPress={props.onPress}>
            <View style={{
                width: size,
                height: size,
                backgroundColor: '#F4401D',
                display: 'flex',
                alignSelf: 'center',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 100
            }}>
                <Icon name={props.name} size={24} color="white" />
            </View>

        </TouchableOpacity>
    )
}