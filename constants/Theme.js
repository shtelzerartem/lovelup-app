const orange = '#F4401D';

const theme = {
    Button: {
        buttonStyle: {
            borderRadius: 100,
            backgroundColor: orange,
            marginBottom: 20
        },
        titleStyle: {
            fontSize: 12,
            paddingTop: 0,
            paddingBottom: 3,
            fontFamily: 'OpenSans-SemiBold'
        }
    },
  };
  
export default theme;  