import React from 'react';
import { createStackNavigator } from 'react-navigation';
import AuthLoading from '../screens/AuthLoading';
import MainScreen from '../screens/MainScreen';
import RegisterScreen from '../screens/RegisterScreen';
import SeekScreen from '../screens/SeekScreen';
import PhotoScreen from '../screens/PhotoScreen';
import ChallengeScreen from '../screens/ChallengeScreen';
import GmailAuth from '../screens/GmailAuth';
import ResultsScreen from '../screens/ResultsScreen';

const MainStack = createStackNavigator(
  {
    Auth: {screen: AuthLoading},
    Main: {screen: MainScreen},
    Register: {screen: RegisterScreen},
    Seek: {screen: SeekScreen},
    Photo: {screen: PhotoScreen},
    Challenge: {screen: ChallengeScreen},
    GmailAuth: {screen: GmailAuth},
    Results: {screen: ResultsScreen}
  },
  {
    initialRouteName: "Auth"
  }
);

export default MainStack;
