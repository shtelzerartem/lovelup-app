import { createBrowserApp } from '@react-navigation/web';

import MainTabNavigator from './MainNavigator';

export default createBrowserApp(MainTabNavigator, { history: 'hash' });
