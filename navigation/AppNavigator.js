import React from 'react';
import { createAppContainer } from 'react-navigation';

import MainTabNavigator from './MainNavigator';

export default createAppContainer(MainTabNavigator);
