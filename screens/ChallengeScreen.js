import React from "react";
import { Text, View, Image, BackHandler, AsyncStorage, StyleSheet } from "react-native";
import { Spinner, Layout } from 'react-native-ui-kitten';
import { vw, vh } from 'react-native-expo-viewport-units';
import CamIcon from '../assets/images/CamIcon';
import Circle from "../assets/images/CircleInactive";
import { MyToken } from "../methods";
import { ws, questState, createWs, setQuestState } from "./globals";

export default class ChallengeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.onBackButtonPressed = this.onBackButtonPressed.bind(this);
    }
    
    render() {
        return (
            questState.questStarted ?
            <View
            style={{
                width: vw(100),
                height: vh(100),
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
            }}
            >
                {/* Challenge progress */}
                <View
                style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "flex-end",
                    marginBottom: 6,
                    width: vw(80)
                }}
                >
                    <Circle active={questState.taskNumber > 0}/>
                    <Circle active={questState.taskNumber > 1}/>
                    <Circle active={questState.taskNumber > 2}/>
                    <Circle active={questState.taskNumber > 3}/>
                </View>
            
                {/* Challenge image */}
                <Image
                style={{
                    width: vw(80), 
                    height: vh(25),
                    borderRadius: 10,
                }}
                source={require('../assets/images/test-challenge.png')}
                />

                {/* Challenge text */}
                <Text
                style={{
                    width: vw(80),
                    marginTop: 30,
                    textAlign: "center",
                    fontFamily: "OpenSans-Regular",
                    fontSize: 16,
                    marginBottom: 30
                }}
                >
                    {questState.task.description}
                    {"\n"}
                    {"\n"}
                    {questState.showMistake ? "Our little machine learning model thinks you didn't get it right, try again :C" : ""}
                </Text>

                {/* Camera icon */}
                <CamIcon onPress={() => this.props.navigation.navigate("Photo")}/>
            </View> :
            <View
            style={{
                width: vw(100),
                height: vh(100),
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
            }}
        >
            <Text style={styles.title}>Searching for partner</Text>
            <Layout style={styles.container}>
                <Spinner status='danger' size="giant"/>
            </Layout>
        </View>
            );
        }
        
    componentDidMount = async () => {
        BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressed);

        this.props.navigation.addListener(
            'didFocus',
            payload => {
              this.forceUpdate();
            }
        );

        const token = await MyToken();
        const sex = await AsyncStorage.getItem("preferred_gender");
        const min_age = await AsyncStorage.getItem("min_age");
        const max_age = await AsyncStorage.getItem("max_age");

        createWs(token, sex, min_age, max_age);

        console.log("ws created");

        ws.onopen = () => {
            console.log("ws opened");
        }

        ws.onmessage = (e) => {
            console.log(e.data);
            var data = JSON.parse(e.data);
            if(data.message_type === "pair_found") {
                console.log("Pair found!")
                setQuestState({
                    ...questState,
                    questStarted: true
                })
                this.forceUpdate();
            } else if(data.message_type === "task") {
                setQuestState({
                    ...questState,
                    taskNumber: questState.taskNumber + 1,
                    task: {
                        id: data.Message.Task.id,
                        description: data.Message.Task.content
                    },
                    showMistake: false
                })
                this.forceUpdate();
            } else if(data.message_type === "result") {
                this.props.navigation.navigate("Results");
            }
        }
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressed);
    }
    
    onBackButtonPressed() {
        return true;
    }
}

const styles = StyleSheet.create({
    title: {
        fontFamily: 'OpenSans-Bold', 
        marginBottom: 50, fontSize: 25, 
        textAlign: 'center'
    },
});
    
ChallengeScreen.navigationOptions = {
    header: null,
};
    