export var questState = {
    questStarted: false,
    tasksAmount: 4, // TODO: get from server
    taskNumber: 0,
    task: {
        id: "",
        description: "",
    },
    showMistake: false
}

export var ws = null;

export function createWs(token, sex, min_age, max_age) {
    ws = new WebSocket(`wss://lootspot.net/api/api/quest?Bearer=${token}&questID=` +
            `5dd0e72c80f65b8f6f27b81c&preferredGender=${sex}&ageMin=${min_age}&ageMax=${max_age}`);
}

export function setQuestState(newState) {
    questState = newState;
}