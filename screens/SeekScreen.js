import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    Dimensions,
    Alert,
    AsyncStorage
} from 'react-native';
import { vw, vh } from 'react-native-expo-viewport-units';
import { Radio, Layout, Input } from 'react-native-ui-kitten';
import { Button } from 'react-native-elements';
import MultiSlider  from '@ptomasroos/react-native-multi-slider';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Marker = props => {
    return (
        <View style={{borderRadius: 100, width: 22, height: 22, position: 'relative', backgroundColor: '#F4401D'}}>
            <Text style={{position: 'absolute', marginTop: -20, marginLeft: 3, color: '#333', zIndex: 999, width: 22}}>{props.value}</Text>
        </View>
    )
}

export default class SeekScreen extends Component {
    state = {
        sex: 0,
        values: [18, 70],
    }

    componentWillMount = async () => {
        // Try to get already existing values
        try {
            var sex = await AsyncStorage.getItem("preferred_gender");
            var min_age = await AsyncStorage.getItem("min_age");
            var max_age = await AsyncStorage.getItem("max_age");

            sex = (sex === null ? this.state.sex : +sex);
            values = (max_age === null || min_age === null ? this.state.values : [+min_age, +max_age]);
            this.setState({sex: sex, values: values})
        } catch (error) {
            Alert.alert(error.message);
        }
    }

    confirm = () => {
        const { sex, values } = this.state;
        try {
            AsyncStorage.setItem('preferred_gender', sex+"");
            AsyncStorage.setItem('min_age', values[0]+"");
            AsyncStorage.setItem('max_age', values[1]+"");
            this.props.navigation.navigate("Main");
        } catch (error) {
            Alert.alert("Somthing went wrong with DB!", error.message);
        }
    }
    
    multiSliderValuesChange = (values) => {
        this.setState({
            values,
        });
    }

    onChange = (value, name) => {
        this.setState({[name]: value})
    }

    changeSex = (value) => {
        this.setState({sex: value})
    }

    render() {
       
        return (
            <View style={styles.view}>
                <Layout style={styles.container}>
                    <Text style={styles.title}>Tell us about who you want to find!</Text>
                    <Text style={styles.sectionName}>Desired age</Text>
                    <View style={{display: 'flex', alignItems: 'center'}}>
                        <MultiSlider
                            isMarkersSeparated={true}
                            sliderLength={Dimensions.get('window').width-70}
                            values={[this.state.values[0], this.state.values[1]]}
                            onValuesChange={this.multiSliderValuesChange}
                            min={18}
                            max={70}
                            step={1}
                            customMarkerLeft={(e) => {return <Marker value={e.currentValue} />}}
                            customMarkerRight={(e) => {return <Marker value={e.currentValue} />}}
                            allowOverlap={true}
                            selectedStyle={{backgroundColor: '#F4401D'}}
                        />
                    </View>
                    <Text style={styles.sectionName}>Desired gender</Text>
                    <Radio
                        style={styles.radio}
                        text='Male'
                        checked={this.state.sex === 1}
                        onChange={() => this.changeSex(1)}
                        status="danger"
                    />
                    <Radio
                        style={styles.radio}
                        text='Female'
                        checked={this.state.sex === 0}
                        onChange={() => this.changeSex(0)}
                        status="danger"
                    />
                    {/* <Radio
                        style={styles.radio}
                        text='Others'
                        checked={this.state.sex === 'others'}
                        onChange={() => this.changeSex('others')}
                    /> */}
                    <TouchableOpacity style={styles.button} onPress={() => this.confirm()}>
                        <Button title="CONTINUE" /> 
                    </TouchableOpacity> 
                </Layout>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#fff',
        height: vh(100),
        padding: 20,
        display: "flex",
        justifyContent: "center"
    },
    title: {
        fontFamily: 'OpenSans-Bold', 
        marginBottom: 50, fontSize: 25, 
        textAlign: 'center'
    },
    sectionName: {
        fontFamily: 'OpenSans-Bold', 
        marginBottom: 20, fontSize: 17, 
    },
    container: {
        // paddingTop: vh(10)
    },
    radio: {
        marginBottom: 15,
    },
    input: {
        marginBottom: 20,
        fontSize: 15
    },
    button: {
        marginTop: 20,
        marginLeft: (vw(100)-vw(60)-40)/2,
        marginRight: (vw(100)-vw(60)-40)/2,
        width: vw(60),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    }
});

SeekScreen.navigationOptions = {
    header: null,
};
