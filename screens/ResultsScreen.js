import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Platform,
    BackHandler
} from 'react-native';
import AppIcon from '../assets/images/AppIcon';
import { Button } from 'react-native-elements';
import { vw, vh } from 'react-native-expo-viewport-units';
import { getMe } from '../methods';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Container = props => <View><View style={{ alignSelf: 'center', paddingHorizontal: 12, minWidth: vw(60)}}>{props.children}</View></View>

export default class ResultsScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <View style={styles.view}>
                <View
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: vh(23),
                    }}
                >
                    <AppIcon />
                </View>
                
                <View
                    style={{
                        position: 'absolute',
                        height: vh(54),
                        width: vw(100),
                        bottom: 0,
                    }}
                >
                    <Text style={{fontFamily: 'OpenSans-Bold', fontSize: 25, textAlign: 'center', marginTop: vh(5)}}>Congratulations!</Text>
                    <Text style={{fontFamily: 'OpenSans-Regular', textAlign: 'center'}}>
                        Enjoy time with your partner C:
                    </Text>
                    <View style={{marginTop: vh(5), paddingHorizontal: vw(20), position: "relative"}}>
                        <TouchableOpacity style={styles.shadow}>
                        <Button
                            title="<з"
                            onPress={() => BackHandler.exitApp()}
                        />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#fff',
        height: vh(100),
    },
    shadow: Platform.OS === "ios" ? {
        shadowColor: "#000",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        borderRadius: 20
    } : {
        marginBottom: 20,
        maxHeight: 39,
        elevation: 2,
        backgroundColor: "rgba(244, 64, 29, 0.1)",
        borderRadius: 20
    }
});

ResultsScreen.navigationOptions = {
    header: null,
};
