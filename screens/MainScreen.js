import React from "react";
import { StyleSheet, Text, View, Image, BackHandler, TouchableOpacity, Platform } from "react-native";
import MapView from "react-native-maps";
import { vw, vh } from 'react-native-expo-viewport-units';
import { LinearGradient } from 'expo-linear-gradient';
import IconButton from '../assets/images/IconButton';
import { Button } from 'react-native-elements';

export default class MainScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            chosenQuest: undefined
        }
        this.colors = ['rgba(256,256,256, 0.01)', 'rgba(256,256,256, 0.4)', 'rgba(256,256,256, 0.8)', '#fff']
        this.onBackButtonPressed = this.onBackButtonPressed.bind(this);
    }

    _renderQuest() {
      if(this.state.chosenQuest === undefined) {
          return null;
      }
      return (
        <LinearGradient colors={this.colors} style={styles.LinearGradientBox}>
            <View style={{ paddingLeft: 25, width: vw(100)-25, display: 'flex', flexDirection: 'row', height: "30%" }}>
              <Image style={styles.EventImage} source={require('../assets/images/junction-pic.png')}/>
              <View
                  style={{
                      flex: 8,
                      display: 'flex',
                      flexDirection: "column",
                      justifyContent: 'center',
                      alignContent: 'stretch',
                      marginLeft: 20,                  }}
              >
                <Text style={styles.EventTitle}>Meet the Hackers</Text>
                <Text style={styles.EventDescription}>Have fun exploring{Platform.OS !== "ios" ? "\n" : null}Junction venue!</Text>
                <TouchableOpacity style={styles.shadow}>
                  <Button
                    title="Let's go!"
                    onPress={() => this.props.navigation.navigate("Challenge")}
                  />
                </TouchableOpacity>
              </View>
          </View>
        </LinearGradient>
      )
  }

  render() {
    return (
      <View style={{width: vw(100), height: vh(100)}}>
        <MapView
          style={{flex: 1}}
          initialRegion={{
            latitude: 60.1860135,
            longitude: 24.8235373,
            latitudeDelta: 0.005,
            longitudeDelta: 0.005
          }}
          showsUserLocation={true}
        >
          <MapView.Marker
            coordinate={{
              latitude: 60.1860135,
              longitude: 24.8235373,
            }}
            image={require("../assets/images/marker-small.png")}
            onPress={() => this.setState({chosenQuest: this.state.chosenQuest === undefined ? 1 : undefined})}
          />
        </MapView>
        <View
          style={{
            position: "absolute",
            bottom: 0,
            width: vw(100),
            height: vh(20),
          }}
        >
          <LinearGradient
              colors={['rgba(255, 255, 255, 0.1)', 'rgba(255, 255, 255, 1)', 'rgba(255, 255, 255, 1)', 'rgba(255, 255, 255, 1)']}
              style={{
                shadowColor: 'white',
                width: "100%",
                height: "100%",
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "flex-end",
                paddingBottom: 10,
                paddingHorizontal: 25,
                shadowOffset: { width: 0, height: 2.5 },
                shadowOpacity: 0.3,
                shadowRadius: 4
              }}
          >
            <IconButton name="filter-list"/>

            <Text style={styles.JunctionStyle}>Junction 2019</Text>

            <IconButton name="settings" onPress={() => this.props.navigation.navigate("Register")}/>
          </LinearGradient>
        </View>
        {this._renderQuest()}
      </View>
    );
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressed);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressed);
  }

  onBackButtonPressed() {
    this.setState({chosenQuest: undefined});
    return true;
  }
}

const styles = StyleSheet.create({
    shadow: Platform.OS === "ios" ? {
        display: 'flex',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        borderRadius: 20,
        alignSelf: 'flex-start', 
        minWidth: "50%",
        marginBottom: -14,
        marginTop: -20
    } : {width: "60%", marginBottom: -10},
    LinearGradientBox: {
        position: 'absolute',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-end',
        bottom: vh(12),
        width: vw(100),
        height: vh(40),
    },
    EventImage: {
        width: vw(33), 
        height: vh(11),
        flex: 4,
        borderRadius: 10,
    },
    EventTitle: {
      fontFamily: "OpenSans-Bold",
      fontSize: 16,
      lineHeight: 16,
      color: "#333333",
      flex: 1,
      height: 15,
      maxHeight: 16
    },
    EventDescription: Platform.OS === "ios" ? {
      fontFamily: "OpenSans-SemiBold",
      fontSize: 12,
      lineHeight: 12,
      color: "#333333",
      flex: 1,
      height: 12,
      marginTop: 5
    } : {
      fontFamily: "OpenSans-SemiBold",
      fontSize: 12,
      lineHeight: 12,
      color: "#333333",
      flex: 1,
      height: 12,
      marginBottom: -20
    },
    JunctionStyle: {
      fontFamily: "OpenSans-Regular",
      fontSize: 16,
      color: "#333333",
      flex: 8,
      textAlign: "center",
      paddingBottom: 20
    }
});

MainScreen.navigationOptions = {
    header: null,
};
