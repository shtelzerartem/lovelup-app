import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Animated,
    Easing,
    AsyncStorage,
    TouchableOpacity,
    Platform
} from 'react-native';
import AppIcon from '../assets/images/AppIcon';
import { Button } from 'react-native-elements';
import { vw, vh } from 'react-native-expo-viewport-units';
import { getMe } from '../methods';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Container = props => <View><View style={{ alignSelf: 'center', paddingHorizontal: 12, minWidth: vw(60)}}>{props.children}</View></View>

export default class AuthLoading extends Component {
    constructor(props) {
        super(props);
        this.moveLogoValue = new Animated.Value(0);
        this.opacityValue = new Animated.Value(0);
    }

    _retrieveData = async () => {
        try {
          const value = await AsyncStorage.getItem('Token');
          const serverValue = await getMe();
          return serverValue;
        } 
        catch (error) {
          return null;
        }
    };

    componentDidMount() {
        this._retrieveData().then((data) => {
            if (data === undefined) {
                this.moveLogo();
                this.fadeIn();
            }
            else {
                this.props.navigation.navigate("Main");
            }
        })
    }

    fadeIn () {
        this.opacityValue.setValue(0);
        Animated.timing(
            this.opacityValue,
            {
              toValue: 1,
              duration: 1000,
              delay: 1500,
              easing: Easing.ease
            }
        ).start()
    }

    moveLogo () {
        this.moveLogoValue.setValue(0);
        Animated.timing(
          this.moveLogoValue,
          {
            toValue: 1,
            duration: 1000,
            delay: 1000,
            easing: Easing.ease
          }
        ).start()
    }

    render() {
        const height = this.moveLogoValue.interpolate({
            inputRange: [0, 1],
            outputRange: [vh(100), vh(80)]
        })
      
        const scale = this.moveLogoValue.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0.7]
        })
        
        const opacity = this.opacityValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 0.5, 1]
        })

        return (
            <View style={styles.view}>
                <Animated.View
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        flexDirection: 'row',
                        height: height,
                        alignItems: 'center',
                        // marginTop: vh(-6),
                        transform: [{scale: scale}],
                    }}
                >
                    <AppIcon />
                </Animated.View>
                
                <Animated.View
                    style={{
                        display: !opacity ? 'none' : 'flex',
                        opacity: opacity,
                        position: 'absolute',
                        height: vh(54),
                        width: vw(100),
                        bottom: 0,
                    }}
                >
                    <Text style={{fontFamily: 'OpenSans-Bold', fontSize: 25, textAlign: 'center', marginTop: vh(5)}}>Lovel Up</Text>
                    <Text style={{fontFamily: 'OpenSans-Regular', textAlign: 'center'}}>
                        is your top-notch{"\n"}
                        dating experience
                    </Text>
                    <View style={{marginTop: vh(5), paddingHorizontal: vw(20), position: "relative"}}>
                        <TouchableOpacity style={styles.shadow}>
                        <Button
                            icon={<Icon name="email" size={22} color="white" style={{marginRight: 10}} />}
                            title="Log in with e-mail"
                            onPress={() => this.props.navigation.navigate("GmailAuth")}
                        />
                        </TouchableOpacity>
                        {/* <TouchableOpacity style={styles.shadow}>
                        <Button
                            icon={<Icon name="facebook-square" size={22} color="white" style={{marginRight: 10}} />}
                            title="Login with Facebook"
                            onPress={() => this.props.navigation.navigate("Main")}
                        />
                        </TouchableOpacity> */}
                    </View>
                </Animated.View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#fff',
        height: vh(100),
    },
    shadow: Platform.OS === "ios" ? {
        shadowColor: "#000",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        borderRadius: 20
    } : {
        marginBottom: 20,
        maxHeight: 39,
        elevation: 2,
        backgroundColor: "rgba(244, 64, 29, 0.1)",
        borderRadius: 20
    }
});

AuthLoading.navigationOptions = {
    header: null,
};
