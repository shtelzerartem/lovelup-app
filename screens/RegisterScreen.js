import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    Dimensions,
    Alert,
    AsyncStorage,
    TouchableOpacity
} from 'react-native';
import { vw, vh } from 'react-native-expo-viewport-units';
import { Radio, Layout, Input } from 'react-native-ui-kitten';
import { Button } from 'react-native-elements';
import MultiSlider  from '@ptomasroos/react-native-multi-slider';
import { updateUser } from '../methods';

const Marker = props => {
    return (
        <View style={{borderRadius: 100, width: 22, height: 22, position: 'relative', backgroundColor: '#F4401D'}}>
            <Text style={{position: 'absolute', marginTop: -20, marginLeft: 3, color: '#333', zIndex: 999, width: 22}}>{props.value}</Text>
        </View>
    )
}

export default class RegisterScreen extends Component {
    
    state = {
        error: false,
        name: '',
        sex: 1,
        age: [18],
    }

    componenDidMount = () => {
        this.loadData();
    }

    loadData = async() => {
        // Try to get already existing values
        try {
            let name = await AsyncStorage.getItem("name");
            let sex = await AsyncStorage.getItem('gender');
            let age = await AsyncStorage.getItem('age');

            name = (name === null ? this.state.name : name);
            sex = (sex === null ? this.state.sex : sex);
            age = (age === null ? this.state.age : age);

            this.setState({name: name, sex: sex, age: age}, () =>    console.log(this.state))
        } catch (error) {
            Alert.alert(error.message);
        }
    }

    oneSliderChange = values => {
        this.setState({
            age: values,
        });
    }

    onChange = (value, name) => {
        if (name === 'name' && this.state.error) this.setState({error: false})
        this.setState({[name]: value});
    }

    changeSex = (value) => {
        this.setState({sex: value})
    }

    validate = () => {
        let _error = false;
        if (!this.state.name) {
            _error = true;
            this.setState({error: true})
            this.input.focus();
        }
        return _error
    }

    saveState = async(state) => {
        try {
            AsyncStorage.setItem('register_state', state);
        } catch (error) {
            Alert.alert("Somthing went wrong with DB!", error.message);
        }
    }
    
    confirm = () => {
        if (!this.validate()) {
            // Pack the data
            const { age, sex, name } = this.state;
            let data = {
                user: {
                    gender: !!sex,
                    age: age[0],
                    name: name
                }
            }

            this.saveState(JSON.stringify(data.user));
            
            // // Save to settings cache
            try {
                // AsyncStorage.setItem('register_state', JSON.stringify(state));
                AsyncStorage.setItem('name', name);
                AsyncStorage.setItem('gender', sex+"");
                AsyncStorage.setItem('age', age[0]+"");
            } catch (error) {
                Alert.alert("Somthing went wrong with DB!", error.message);
            }

            // Send update to server
            updateUser(data).then(res => {
                if (res.status !== 200) Alert.alert("Something went wrong!");
                else {
                    if (res.data.error) Alert.alert(res.data.error); 
                    else this.props.navigation.navigate("Seek");
                }
            })
        }
    }
    
    render() {
       
        return (
            <View style={styles.view}>
                <Layout style={styles.container}>
                    <Text style={styles.title}>Tell us about you!</Text>
                    <Text style={styles.sectionName}>Your name</Text>
                    <Input
                        ref={(input) => { this.input = input; }}
                        style={styles.input}
                        status={this.state.error ? 'danger': 'warning'}
                        placeholder='Name'
                        value={this.state.name}
                        onChangeText={(value) => this.onChange(value, 'name')}
                    />
                    <Text style={styles.sectionName}>Your age</Text>
                    <View style={{display: 'flex', alignItems: 'center'}}>
                        <MultiSlider
                            isMarkersSeparated={true}
                            sliderLength={Dimensions.get('window').width-70}
                            values={this.state.age}
                            onValuesChange={this.oneSliderChange}
                            min={18}
                            max={70}
                            step={1}
                            customMarkerLeft={(e) => {return <Marker value={e.currentValue} />}}
                            allowOverlap={true}
                            selectedStyle={{backgroundColor: '#F4401D'}}
                        />
                    </View>
                    <Text style={styles.sectionName}>Your gender</Text>
                    <Radio
                        style={styles.radio}
                        text='Male'
                        checked={this.state.sex === 1}
                        onChange={() => this.changeSex(1)}
                        status="danger"
                    />
                    <Radio
                        style={styles.radio}
                        text='Female'
                        checked={this.state.sex === 0}
                        onChange={() => this.changeSex(0)}
                        status="danger"
                    />
                    {/* <Radio
                        style={styles.radio}
                        text='Others'
                        checked={this.state.sex === 'others'}
                        onChange={() => this.changeSex('others')}
                    /> */}
                    <TouchableOpacity style={styles.button}>
                        <Button title="CONTINUE" onPress={this.confirm} /> 
                    </TouchableOpacity> 
                </Layout>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#fff',
        height: vh(100),
        padding: 20,
        display: "flex",
        justifyContent: "center"
    },
    title: {
        fontFamily: 'OpenSans-Bold', 
        marginBottom: 50, fontSize: 25, 
        textAlign: 'center'
    },
    sectionName: {
        fontFamily: 'OpenSans-Bold', 
        marginBottom: 20, fontSize: 17, 
    },
    container: {
        // paddingTop: vh(10)
    },
    radio: {
        marginBottom: 15,
    },
    input: {
        marginBottom: 20,
        fontSize: 15,
        borderColor: '#F4401D'
    },
    button: {
        marginTop: 20,
        marginLeft: (vw(100)-vw(60)-40)/2,
        marginRight: (vw(100)-vw(60)-40)/2,
        width: vw(60),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    }
});

RegisterScreen.navigationOptions = {
    header: null,
};
