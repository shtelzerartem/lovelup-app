import React from "react";
import { StyleSheet, Text, View, Image, SafeAreaView, ImageBackground } from "react-native";
import { vw, vh } from 'react-native-expo-viewport-units';
import { LinearGradient } from 'expo';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import IconMenu from "../assets/images/IconButton";
import { TouchableOpacity } from "react-native";
import { ws, questState } from "./globals";
import { sendPicture } from "../methods";

export default class PhotoScreen extends React.Component {
    state = {
        hasCameraPermission: null,
        type: Camera.Constants.Type.back,
        picture: null,
        pictureBase64: null
    };

    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        console.log(status);
        this.setState({ hasCameraPermission: status === 'granted'});
    }

    async takePicture() {
        if (this.camera) {
            const options = { quality: 1, base64: true, fixOrientation: true, exif: true};
            await this.camera.takePictureAsync(options).then(photo => {
                this.setState({picture: photo.uri, pictureBase64: photo.base64})
                photo.exif.Orientation = 1;            
                // console.log(photo);            
            }).catch(error => {
                console.log(error);
            })
        }
    }

    submitImage = async () => {
        var res = await sendPicture(questState.task.id ,this.state.pictureBase64);
        console.log("RESULT:");
        console.log(res);
        if(res === undefined) {
            console.log("you didnt get it right")
            questState.showMistake = true;
        }
        if(res === undefined || questState.taskNumber < 4){
            this.props.navigation.navigate("Challenge");
        }
    }

    flipCamera = () => {
        this.setState({
            type:
              this.state.type === Camera.Constants.Type.back
                ? Camera.Constants.Type.front
                : Camera.Constants.Type.back,
          });
    }

    render() {
        const { hasCameraPermission } = this.state;
        if (hasCameraPermission === null) {
        return <View />;
        } else if (hasCameraPermission === false) {
        return <Text>No access to camera</Text>;
        
        } else if (this.state.picture === null) {
            return (
            <View
                style={{
                    width: vw(100),
                    height: vh(100)
                }}
            >
                <Camera ref={cam => this.camera = cam} style={{ flex: 1 }} type={this.state.type} ratio={"2:1"}>
                    <SafeAreaView style={styles.footer}>
                        <TouchableOpacity
                            onPress={this.takePicture.bind(this)}
                        >
                            <View style={{display: 'flex', borderRadius: 80, backgroundColor: '#333', width: 80, height: 80, marginBottom: 10, justifyContent: 'center'}}>
                                <View style={styles.shootButton} />
                            </View>
                        </TouchableOpacity>
                        <IconMenu name="arrow-back" size="big" onPress={() => this.props.navigation.navigate("Challenge")} style={{position: 'absolute', left: 10, bottom: (10+80-48)/2}} />
                        <IconMenu name="flip" size="big" onPress={this.flipCamera} style={{position: 'absolute', right: 10, bottom: (10+80-48)/2}} />
                    </SafeAreaView>
                </Camera>
            </View>
            );
        }
        else {
            return (
                <ImageBackground
                    source={{ uri: this.state.picture }}
                    style={styles.preview}
                >
                    <View style={{position: 'absolute', left: 10, bottom: 10, width: vw(100) - 20, display: 'flex', flexDirection: 'row'}}>
                        <IconMenu name="close" size="big" onPress={() => this.setState({picture: null})} />
                        <IconMenu name="done" size="big" onPress={() => this.submitImage()}  style={{position: 'absolute', right: 0, bottom: 0}} />
                    </View>
                </ImageBackground>
            )
        }
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#fff',
        height: vh(100),
    },
    header: {
        position: 'absolute',
        top: 0,
        width: vw(100),
        flex: 1,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        paddingHorizontal: 20
    },
    footer: {
        position: 'absolute',
        width: vw(100),
        bottom: 0,
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        padding: 20,
        height: vh(15),
    },
    preview: {
        width: vw(100),
        height: vh(100)
    },
    shootButton: {
        display: 'flex',
        width: 70,
        height: 70,
        backgroundColor: '#F4401D',
        alignSelf: 'center',
        borderRadius: 70,
    }
});


PhotoScreen.navigationOptions = {
    header: null,
};
