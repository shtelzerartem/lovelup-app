import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Alert, AsyncStorage
} from 'react-native';
import { vw, vh } from 'react-native-expo-viewport-units';
import { Layout, Input } from 'react-native-ui-kitten';
import { Button } from 'react-native-elements';
import { register } from '../methods';


export default class GmailAuth extends Component {
    
    state = {
        checked: false,
        email: '',
        password: '',
        errorEmail: false,
        errorPassword: false,
        dialog: false,
    }
    
    componentDidMount() {
        if (this.input) this.input.focus()
    }
    
    onChange = (value, name) => {
        if (name === 'email' || name === 'password' && !this.state.errorEmail || !this.state.errorPassword) this.setState({...this.state, errorEmail: false, errorPassword: false})
        this.setState({[name]: value});
    }
    
    validateEmail(email) {
        var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(email);
    }

    validate = () => {
        const { email, password } = this.state;
        let _error = false;
        if (!password) {
            _error = true;
            this.setState({errorPassword: true});
            this.password.focus();
        }
        if (!this.validateEmail(email)) {
            _error = true;
            this.setState({errorEmail: true});
            this.email.focus();
        }
        return _error;
    }

    confirm = () => {
        if (!this.validate()){
            const {email, password} = this.state;
            let data = {
                email: email,
                password: password
            }
            register(data).then(res => {
                if (res.status !== 200) Alert.alert("Somthing went wrong!") 
                else {
                    if (res.data.error) {
                        Alert.alert(res.data.error); 
                    }
                    else{
                        this.saveToken(res.data.token)
                    }
                }
            })
        }
    }

    saveToken = async(Token) => {
        try {
            console.log(Token);
            AsyncStorage.setItem('Token', Token);
            this.props.navigation.navigate("Register");
        } catch (error) {
            Alert.alert("Somthing went wrong with DB!", error.message);
        }
    }

    render() {
       
        return (
            <View style={styles.view}>
                <Layout style={styles.container}>
                    <Text style={styles.title}>Proceed with e-mail</Text>
                    <Text style={styles.sectionName}>Your e-mail</Text>
                    <Input
                        ref={(input) => { this.email = input; }}
                        style={styles.input}
                        name="email"
                        keyboardType="email-address"
                        status={this.state.error ? 'warning' : 'danger'}
                        placeholder='Email'
                        value={this.state.email}
                        onChangeText={(value) => this.onChange(value, 'email')}
                    />
                    <Text style={styles.sectionName}>Create a password</Text>
                    <Input
                        ref={(input) => { this.password = input; }}
                        style={styles.input}
                        name="password"
                        keyboardType="default"
                        type="password"
                        secureTextEntry={true}
                        status={this.state.error ? 'warning' : 'danger'}
                        placeholder='Password'
                        value={this.state.password}
                        onChangeText={(value) => this.onChange(value, 'password')}
                    />
                    <View style={styles.button}>
                        <Button
                            title="SUBMIT"
                            onPress={() => this.confirm()}
                        /> 
                    </View> 
                </Layout>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#fff',
        height: vh(100),
        padding: 20,
        display: "flex",
        justifyContent: "center"
    },
    title: {
        fontFamily: 'OpenSans-Bold', 
        marginBottom: 50, fontSize: 25, 
        textAlign: 'center'
    },
    sectionName: {
        fontFamily: 'OpenSans-Bold', 
        marginBottom: 20, fontSize: 17, 
    },
    container: {
        // paddingTop: vh(10)
    },
    input: {
        marginBottom: 20,
        fontSize: 15
    },
    button: {
        marginTop: 20,
        marginLeft: (vw(100)-vw(60)-40)/2,
        marginRight: (vw(100)-vw(60)-40)/2,
        width: vw(60),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    }
});

GmailAuth.navigationOptions = {
    header: null,
};
